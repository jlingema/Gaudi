/** @file VirtualDestructors.cpp
 *
 * This file is used to provide virtual destructors for abstract interfaces.
 * They need to be defined in the linker library to make the dynamic_cast work.
 *
 * @author Marco Clemencic
 *
 */
